﻿using Lab1.Subject;
using System;

namespace Lab1.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Wait a moment, please");
            Teacher teacherOne = new Teacher()
            {
                Type = TeacherType.Lecture,
                TeacherName = "Volodimir",
                TeacherSurname = "Mikolayovich",
                TeacherDescription = "Lector of .NET, good man",
            };

            Teacher teacherTwo = new Teacher()
            {
                Type = TeacherType.Assistant,
                TeacherName = "Andrusenko",
                TeacherSurname = "Yulia",
                TeacherDescription = "Assistant of .Net, cool person",
            };

            Subject.Subject dotnet = new Subject.Subject()
            {
                Credits = 6,
                Name = "DotNet",
                Hours = 180,
            };

            Place place = new Place()
            {
                Name = "Classroom",
            };

            Console.WriteLine("Done!");
        }
    }
}

