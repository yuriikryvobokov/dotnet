﻿namespace Lab1.Subject
{
    public class Subject
    {
        public string Name { get; set; }
        public int Credits { get; set; }
        public double Hours { get; set; }
    }
}
