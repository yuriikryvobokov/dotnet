﻿using System;

namespace Lab1.DomainWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain otherDomain = AppDomain.CreateDomain("otherDomain");
            otherDomain.ExecuteAssembly(@"E:\6\1\NET\Lab1\Lab1.ConsoleApp\bin\Debug\Lab1.ConsoleApp.exe");
            AppDomain.Unload(otherDomain);
            Console.ReadLine();
        }
    }
}
