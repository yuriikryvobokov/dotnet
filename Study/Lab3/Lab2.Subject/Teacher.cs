﻿namespace Lab1.Subject
{
    public class Teacher
    {
        public string TeacherName { get; set; }
        public string TeacherSurname { get; set; }
        public TeacherType Type { get; set; }
        public string TeacherDescription { get; set; }
        private bool Experience = false;
        public void experienceOn() => Experience = true;
        public void experienceOff() => Experience = false;
    }
}
