﻿using Lab1.Subject;
using Lab3.Logic;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;

namespace Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            closeKey();
            openKey();
            cryptoSignature();
            cryptoHash();
        }

        static void closeKey()
        {

            TripleDESCryptoServiceProvider tDESalg = new TripleDESCryptoServiceProvider();
            Subject sData = new Subject() { Credits = 6, Name = "DotNet", Hours = 180 };
            var serialized = JsonSerializer.Serialize<Subject>(sData);
            string FileName = "CText.txt";

            Logic.RC2CryptoServiceProvider.EncryptTextToFile(serialized, FileName, tDESalg.Key, tDESalg.IV);

            string Final = Logic.RC2CryptoServiceProvider.DecryptTextFromFile(FileName, tDESalg.Key, tDESalg.IV);
            Subject restoredMechanic = JsonSerializer.Deserialize<Subject>(Final);

            Console.WriteLine(Final);
        }

        static void openKey()
        {
            using (ECDiffieHellmanCngReciver reciever = new ECDiffieHellmanCngReciver())
            {
                using (RSACryptoServiceProvider rsaKey = new RSACryptoServiceProvider())
                {
                    Subject sData = new Subject() { Credits = 6, Name = "DotNet", Hours = 180 };
                    var serialized = JsonSerializer.Serialize<Subject>(sData);
                    // public key
                    rsaKey.ImportCspBlob(reciever.key);
                    byte[] encryptedSessionKey = null;
                    byte[] encryptedMessage = null;
                    byte[] iv = null;
                    ECDiffieHellmanCngService.Send(rsaKey, serialized, out iv, out encryptedSessionKey, out encryptedMessage);
                    reciever.Receive(iv, encryptedSessionKey, encryptedMessage);
                }
            }
        }

        static void cryptoSignature()
        {
            Subject sData = new Subject() { Credits = 6, Name = "DotNet", Hours = 180 };
            var serialized = JsonSerializer.Serialize<Subject>(sData);
            byte[] data = Encoding.ASCII.GetBytes(serialized);
            var rsa = new RSACryptoServiceProvider();
            byte[] digitalSignature = rsa.SignData(data, "SHA512");
            string someString = Encoding.ASCII.GetString(digitalSignature);
            Console.WriteLine(someString);
        }

        static void cryptoHash()
        {
            Subject sData = new Subject() { Credits = 6, Name = "DotNet", Hours = 180 };
            var serialized = JsonSerializer.Serialize<Subject>(sData);
            byte[] data = Encoding.ASCII.GetBytes(serialized);
            byte[] result;
            SHA384 shaM = new SHA384Managed();
            result = shaM.ComputeHash(data);
            string someString = Encoding.ASCII.GetString(result);
            Console.WriteLine(someString);
        }
    }
}
