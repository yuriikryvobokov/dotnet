﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace Lab4.Service
{
    public partial class TeacherService : ServiceBase
    {

        Logger logger;
        public TeacherService()
        {
            InitializeComponent();
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            logger = new Logger();
            Thread loggerThread = new Thread(new ThreadStart(logger.Work));
            loggerThread.Start();
        }

        protected override void OnStop()
        {
        }
    }

    class Logger
    {
        const int port = 8888;
        object obj = new object();

        public void RecordEntry(string car)
        {
            lock (obj)
            {
                using (StreamWriter writer = new StreamWriter("E:\\dot net labs\\templog.txt", true))
                {
                    writer.WriteLine($"Car added: {car}");
                    writer.Flush();
                }
            }
        }

        public void Work()
        {
            TcpListener server = null;
            try
            {
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");
                server = new TcpListener(localAddr, port);
                server.Start();

                while (true)
                {
                    TcpClient client = server.AcceptTcpClient();
                    NetworkStream stream = client.GetStream();

                    byte[] data = new byte[64];

                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    RecordEntry(message);
                    stream.Close();
                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (server != null)
                    server.Stop();
            }
        }
    }
}
