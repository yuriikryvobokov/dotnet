﻿using Lab1.Subject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab4.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Teacher> teachers = new ObservableCollection<Teacher>
            {
                new Teacher{ TeacherName="Olesia", TeacherSurname="Yuriivna", TeacherDescription="Teacher of Merz", Type=TeacherType.Lecture},
                new Teacher{ TeacherName="Ivan", TeacherSurname="Ivanov", TeacherDescription="Teacher of subject", Type=TeacherType.Assistant},
            };

        public MainWindow()
        {
            InitializeComponent();
            dgrid.ItemsSource = teachers;
            cType.SelectedIndex = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var car = new Teacher
            {
                TeacherName = cName.Text,
                TeacherSurname = cSurname.Text,
                TeacherDescription = cDescription.Text,
                Type = Enum.Parse<TeacherType>(((TextBlock)cType.SelectedItem).Text),

            };
            teachers.Add(car);
            cName.Text = "";
            cSurname.Text = "";
            cDescription.Text = "";
            cType.SelectedIndex = 0;
            SendMessage(JsonSerializer.Serialize<Teacher>(car));
        }

        private void SendMessage(string message)
        {
           int port = 8888;
           string server = "127.0.0.1";

            try
            {
                TcpClient client = new TcpClient();
                client.Connect(server, port);
                NetworkStream stream = client.GetStream();
                byte[] data = Encoding.UTF8.GetBytes(message);
                stream.Write(data, 0, data.Length);
                stream.Close();
                client.Close();
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }
        
    }
}
