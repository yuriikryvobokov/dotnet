﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

namespace Lab2.ConsoleApp
{ 
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("2.3.3");
            //2.3.3
            string longName = "lab2.subject, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            Assembly assem = Assembly.Load(longName);
            if (assem == null)
                Console.WriteLine("Unable to load assembly...");
            else
                Console.WriteLine(assem.FullName);
            Console.WriteLine();

            //2.3.4
            Console.WriteLine("2.3.4");
            foreach (var cl in assem.DefinedTypes)
            {
                Console.WriteLine($"Type {cl.Name} has methods:");
                foreach(var method in cl.GetMethods())
                {
                    Console.Write(" --> " + method.ReturnType.Name + " \t" + method.Name + "(");
                    ParameterInfo[] p = method.GetParameters();
                    for (int i = 0; i < p.Length; i++)
                    {
                        Console.Write(p[i].ParameterType.Name + " " + p[i].Name);
                        if (i + 1 < p.Length) Console.Write(", ");
                    }
                    Console.Write(")\n");
                }
                Console.WriteLine();
            }

            //2.3.5
            Console.WriteLine("2.3.5");
            Type teacherType = assem.GetType("Lab1.Subject.Subject");
            object teacher = Activator.CreateInstance(teacherType);
            string response = (string)(teacherType.InvokeMember(
                    "isStudying",
                    BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                    null,
                    teacher,
                    null));
            Console.WriteLine(response);
            Console.WriteLine();

            //2.3.6
            Console.WriteLine("2.3.6");
            dynamic t = teacher;
            t.isStudying();
            Console.WriteLine();

            //2.3.7
            Console.WriteLine("2.3.7");
            dynamic obj = new System.Dynamic.ExpandoObject();
            obj.Name = "Dotnet";
            obj.Credits = 6;
            obj.Hours = 180.0;
            obj.isStudying = (System.Action)(()=>Console.WriteLine($"Subject {obj.Name} is studying"));
            obj.isStudying();
            Console.ReadLine();

            //2.3.8
            Console.WriteLine("2.3.8");
            t.Name = obj.Name;
            t.Credits = obj.Credits;
            t.Hours = obj.Hours;

            Application xlApp = new
            Application();
            object misValue = Missing.Value;
            var xlWorkBook = xlApp.Workbooks.Add(misValue);
            var xlWorkSheet = xlWorkBook.Worksheets.get_Item(1);
            var props = teacherType.GetProperties();
            xlWorkSheet.Cells[1, 1] = "Subject";
            xlWorkSheet.Cells[1, 2] = "Studying";
            for (int i=0;i< props.Length; i++)
            {
                xlWorkSheet.Cells[i + 2, 1] = props[i].Name;
                xlWorkSheet.Cells[i + 2, 2] = t.GetType().GetProperty(props[i].Name).GetValue(t, null);
            }
            
            xlWorkBook.SaveAs("1.xls");

            xlWorkBook.Close(0);
            xlApp.Quit();

            Console.Read();
        }
    }
}

