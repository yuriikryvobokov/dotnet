﻿using Lab1.CustomType;
using System;

namespace Lab1.Subject
{
    [User]
    [Obsolete]
    public class Place
    {
        public string Name { get; set; }
    }
}
