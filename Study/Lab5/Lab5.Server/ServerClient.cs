﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Lab5.Server
{
    public class ServerClient
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        public bool IsClient { get; set; }
        public string PairId { get; set; }
        string userName;
        TcpClient client;
        CustomServer server; // объект сервера

        public ServerClient(TcpClient tcpClient, CustomServer serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                string message = GetMessage();
                if (message.StartsWith("~"))
                {
                    message = message.Replace("~", "");
                    IsClient = true;
                }
                else
                    IsClient = false;

                userName = message;
                server.AddToList(this.Id, this.IsClient);
                Console.WriteLine(message + $" - {this.Id} - connected");
               
                while(true)
                {
                    if (!IsClient && !string.IsNullOrEmpty(PairId))
                        break;
                    else
                    {
                        var pairId = server.GetPair(this.Id, this.IsClient);
                        if (!string.IsNullOrEmpty(pairId))
                        {
                            PairId = pairId;
                            break;
                        }
                    }
                }
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = String.Format("{0}: {1}", userName, message);
                        Console.WriteLine($"{Id} -> {message} -> {PairId}");
                        server.BroadcastMessage(message, this.Id, this.PairId);
                    }
                    catch
                    {
                        server.DeletePair(PairId);
                        PairId = null;
                        message = String.Format("{0}: покинул чат", userName);
                        Console.WriteLine(message);
                        server.BroadcastMessage(message, this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        // чтение входящего сообщения и преобразование в строку
        private string GetMessage()
        {
            byte[] data = new byte[64]; // буфер для получаемых данных
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}