﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;

namespace Lab5.Server
{
    public class CustomServer
    {
        static TcpListener tcpListener;
        List<ServerClient> users = new List<ServerClient>();
        List<ServerClient> mechanics = new List<ServerClient>();
        List<ServerClient> clients = new List<ServerClient>();

        protected internal void AddConnection(ServerClient clientObject)
        {
            users.Add(clientObject);
        }
        protected internal void RemoveConnection(string id)
        {
            // получаем по id закрытое подключение
            ServerClient client = users.FirstOrDefault(c => c.Id == id);
            // и удаляем его из списка подключений
            if (client != null)
                users.Remove(client);
        }
        // прослушивание входящих подключений
        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, 8889);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    ServerClient clientObject = new ServerClient(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        // трансляция сообщения подключенным клиентам
        protected internal void BroadcastMessage(string message, string id)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].Id != id) // если id клиента не равно id отправляющего
                {
                    users[i].Stream.Write(data, 0, data.Length); //передача данных
                }
            }
        }

        protected internal void BroadcastMessage(string message, string id, string pairId)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].Id != id && users[i].Id == pairId) // если id клиента не равно id отправляющего
                {
                    users[i].Stream.Write(data, 0, data.Length); //передача данных
                }
            }
        }

        protected internal void AddToList(string id, bool isClient)
        {
            var user = users.First(x => x.Id == id);
            if (isClient)
                clients.Add(user);
            else
                mechanics.Add(user);
        }

        protected internal string GetPair(string id, bool isClient)
        {
            if (isClient)
            {
                var mech = mechanics.FirstOrDefault(x => string.IsNullOrEmpty(x.PairId));
                if (mech != null)
                {
                    mech.PairId = id;
                    Console.WriteLine($"{id} connected with {mech.Id}");
                    return mech.Id;
                }
            }

            return null;
        }

        protected internal string DeletePair(string pairId)
        {
            users.First(x => x.PairId == pairId).PairId = null;

            return null;
        }

        // отключение всех клиентов
        protected internal void Disconnect()
        {
            tcpListener.Stop(); //остановка сервера

            for (int i = 0; i < users.Count; i++)
            {
                users[i].Close(); //отключение клиента
            }
            Environment.Exit(0); //завершение процесса
        }
    }
}