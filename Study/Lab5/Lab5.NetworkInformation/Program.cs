﻿using System;
using System.Net.NetworkInformation;

namespace Lab5.NetworkInformation
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayGatewayAddresses();
        }

        public static void DisplayGatewayAddresses()
        {
            Console.WriteLine("Gateways");
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                GatewayIPAddressInformationCollection addresses = adapterProperties.GatewayAddresses;
                if (addresses.Count > 0)
                {
                    Console.WriteLine(adapter.Description);
                    foreach (GatewayIPAddressInformation address in addresses)
                    {
                        Console.WriteLine($"Gateway Address : {address.Address}");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
