﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;

namespace Lab5.MechApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string user;
        private const string host = "127.0.0.1";
        private const int port = 8889;
        static TcpClient client;
        static NetworkStream stream;
        private Thread ChatThread = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            chatgrid.Visibility = Visibility.Visible;
            startgrid.Visibility = Visibility.Hidden;
            ChatThread = new Thread(new ThreadStart(ThreadFunction));
            ChatThread.Start();
        }
        string GetUserName()
        {
            string text = string.Empty;
            Dispatcher.Invoke(() => text = username.Text);
            return text;
        }
        string GetChatInput()
        {
            string text = string.Empty;
            Dispatcher.Invoke(() =>
            {
                text = chatinput.Text;
                chatoutput.Text = chatoutput.Text + $"\nYou: {chatinput.Text}";
                chatinput.Text = string.Empty;
            });
            return text;
        }

        void AddMessage(string message)
        {
            Dispatcher.BeginInvoke(new ThreadStart(delegate { chatoutput.Text = chatoutput.Text + $"\n{message}"; }));
        }

        private void ThreadFunction()
        {
            user = GetUserName();
            client = new TcpClient();
            try
            {
                client.Connect(host, port);
                stream = client.GetStream();

                string message = user;
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);

                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start();
                AddMessage($"\nДобро пожаловать, {user}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                //Disconnect();
            }
        }

        private void SendMClick(object sender, RoutedEventArgs e)
        {
            byte[] data = Encoding.Unicode.GetBytes(GetChatInput());
            stream.Write(data, 0, data.Length);
        }

        void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64];
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    AddMessage(message);
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
        }
    }
}
