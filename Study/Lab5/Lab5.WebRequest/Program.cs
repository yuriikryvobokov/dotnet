﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;

namespace Lab5.WebRequestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            WebRequest request = WebRequest.Create("https://restcountries.com/v3.1/name/{ukraine}");
            WebResponse response = request.GetResponse();
            string downloadedJson = string.Empty;
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    downloadedJson = reader.ReadToEnd();
                }
            }
            response.Close();
            Console.WriteLine(PrettyJson(downloadedJson));
            Console.Read();
        }

        public static string PrettyJson(string unPrettyJson)
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = true
            };

            var jsonElement = JsonSerializer.Deserialize<JsonElement>(unPrettyJson);

            return JsonSerializer.Serialize(jsonElement, options);
        }
    }
}
